from django.http import HttpResponseForbidden
from main.models import *


class IPBlockMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Отримати IP-адресу користувача
        user_ip = request.META.get('HTTP_X_REAL_IP')
        if user_ip:
            # Якщо є HTTP_X_FORWARDED_FOR, використовуйте перший IP зі списку
            user_ip = user_ip.split(',')[0].strip()
        else:
            # Якщо немає HTTP_X_FORWARDED_FOR, використовуйте REMOTE_ADDR
            user_ip = request.META.get('REMOTE_ADDR')
        print("IP-адреса користувача:", user_ip)

        # Перевірити IP-адресу та заблокувати доступ якщо потрібно
        blocked_ips = BlockedIP.objects.values_list('ip_address', flat=True)
        if user_ip in blocked_ips:
            return HttpResponseForbidden('Access Denied')

        # Продовжити обробку запиту
        response = self.get_response(request)
        return response



