import random

from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import CreateView, View, DetailView
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth import authenticate, login, get_user_model
from django.contrib.auth.views import LoginView
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.mail import send_mail
from django.conf import settings
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.db.models import F

from .models import *
from .forms import *

@user_passes_test(lambda u: u.is_superuser)
def fillbd(request):
    # Генерувати 10 фейкових лікарів
    Doctors.generate_fake_doctors(10)
    # Генерація випадкового розкладу для лікаря
    Schedule.generate_fake_schedule(10)
    #Генеруємо список ліків
    #Medicine.generate_fake_medicines(10)
    Medicine.generate_medicine_data()
    #Генеруємо 10 фейкових відгуків
    UserReview.generate_fake_reviews(5)
    #Генеруємо список новин
    News.generate_news_data()


    print('База даних заповнена')
    return redirect('home')


def delete_old_data():
    threshold_time = datetime.now() - timedelta(hours=24)
    old_orders = Orders.objects.filter(time_create__lt=threshold_time)
    old_orders.delete()
    old_appointments = Appointments.objects.filter(time_create__lt=threshold_time)
    old_appointments.delete()


@user_passes_test(lambda u: u.is_superuser)
def admin(request):
    return redirect('admin/')


def main(request):
    news_post = News.objects.all()
    user_review = UserReview.objects.filter(rating__gte=3, text__isnull=False)[:5]
    context = {'news_post': news_post, 'user_review': user_review}
    return render(request, 'main/main.html', context=context)


def about_us(request):
    return render(request, 'main/about_us.html')


def news(request):
    news_post = News.objects.all()
    return render(request, 'main/news.html', {'news_post': news_post})


def show_news(request, news_slug):
    news = get_object_or_404(News, slug=news_slug)
    context = {'news': news}
    return render(request, 'main/show_news.html', context=context)


def medicine(request):
    medicine_post = Medicine.objects.all()
    context = {'medicine_post': medicine_post}
    return render(request, 'main/medicine.html', context=context)


def user_order(request):
    if request.method == 'GET':
        user_id = request.user.id
        if user_id == None:
            print('User is not authenticated', user_id)
            return JsonResponse({'error': 'Вам потрібно спочатку зареєструватись!'})
        else:
            user = f"{request.user.first_name} {request.user.last_name} {request.user.surname}"
            user_email = request.user.email
            user_phonenumber = request.user.phone_number
            order_location = request.GET.get('order_location')
            order_medicine = request.GET.get('order_medicine')
            order_amount = request.GET.get('order_amount')
            order_price = request.GET.get('order_price')
            order_code = random.randint(100000, 999999)
            print(user, user_email, user_phonenumber, order_code, order_medicine, order_amount, order_price, order_location)
            # send_mail(
            #    'Код веритифікації',
            #    f'{code} - Ваш код для отримання ліків: ',
            #    settings.DEFAULT_FROM_EMAIL,
            #    [email],
            #    fail_silently=False,
            # )

            order = Orders(
                user=user,
                user_email=user_email,
                user_phonenumber=user_phonenumber,
                order_location=order_location,
                order_medicine=order_medicine,
                order_amount=order_amount,
                order_price=order_price,
                order_code=order_code,
                time_create=datetime.now()
            )
            order.save()

            Medicine.objects.filter(medicine=order_medicine).update(amount=F('amount') - int(order_amount))

            print(user, user_email, user_phonenumber, order_code, order_medicine, order_amount, order_price, order_location)
            return JsonResponse({'success': 'Замовлення успішне!'})


def doctors(request):
    doctor_object = Doctors.objects.all()
    doctor_schedule = Schedule.objects.all()
    return render(request, 'main/doctors.html', {'doctor_object': doctor_object, 'doctor_schedule': doctor_schedule})


def user_appointment(request):
    if request.method == 'GET':
        user_id = request.user.id
        if user_id == None:
            print('User is not authenticated', user_id)
            return JsonResponse({'error': 'Вам потрібно спочатку зареєструватись!'})
        else:
            user = f"{request.user.first_name} {request.user.last_name} {request.user.surname}"
            user_email = request.user.email
            day = request.GET.get('day')
            doctor = request.GET.get('doctor')
            doctor_id = request.GET.get('doctor_id')
            doctor_specialization = request.GET.get('doctor_specialization')
            time = request.GET.get('time')
            meet_url = request.GET.get('meet_url')
            code = random.randint(100000, 999999)
            #send_mail(
            #    'Код веритифікації',
            #    f'{code} - Ваш код для запису на онлайн прийом у {day} о {time}. Лікар: {doctor_specialization}, {doctor}',
            #    settings.DEFAULT_FROM_EMAIL,
            #    [email],
            #    fail_silently=False,
            #)

            schedule = Schedule.objects.get(doctor=doctor_id)
            time_parts = time.split(':')
            time_parts = time_parts[0]
            if day == 'Понеділок':
                schedule.monday.remove(time_parts)
            elif day == 'Вівторок':
                schedule.tuesday.remove(time_parts)
            elif day == 'Середа':
                schedule.wednesday.remove(time_parts)
            elif day == 'Четвер':
                schedule.thursday.remove(time_parts)
            elif day == 'П`ятниця':
                schedule.friday.remove(time_parts)
            elif day == 'Субота':
                schedule.saturday.remove(time_parts)
            elif day == 'Неділя':
                schedule.sunday.remove(time_parts)

            schedule.save()
#
            appointment = Appointments(
                user=user,
                user_email=user_email,
                doctor=doctor,
                doctor_specialization=doctor_specialization,
                day=day,
                time=time,
                meet_url=meet_url,
                code=code,
                time_create = datetime.now()
            )
            appointment.save()

            print(user, user_email, code, day, time, doctor, doctor_specialization, meet_url)
            return JsonResponse({'success': 'Лікар буде очікувати вас о ' +  time})


class LoginView(LoginView):
    template_name = 'main/login.html'
    form_class = CustomAuthenticationForm
    success_url = reverse_lazy('office')

    def form_valid(self, form):
        # Check if the email and password are correct
        username = form.cleaned_data.get['username']
        password = form.cleaned_data.get['password']
        user = authenticate(email=username, password=password)
        if user is not None:
            login(self.request, user)
            if user.user_verified:
                return redirect(self.success_url)
            else:
                return redirect('verification')
        elif CustomUser.objects.filter(email=username).exists():
            # Show error message if login failed due to incorrect password
            form.add_error('password', 'Невірний пароль')
        else:
            # Show error message if login failed due to unknown email
            form.add_error('username', 'Адреса не зареєстрована')
            return super().form_invalid(form)

    def form_invalid(self, form):
        messages.error(self.request, 'Авторизація невдала')
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_errors'] = context['form'].errors
        return context


class SignupView(CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'main/signup.html'

    def form_valid(self, form):
        # check if the user with this email already exists
        email = form.cleaned_data.get('email')
        phone_number = form.cleaned_data.get('phone_number')
        if CustomUser.objects.filter(email=email).exists():
            form.add_error('email', 'Ця електронна адреса вже зареєстрована')
            return super().form_invalid(form)
        elif CustomUser.objects.filter(phone_number=phone_number).exists():
            form.add_error('phone_number', 'Цей номер телефону вже зареєстрований')
            return super().form_invalid(form)
        else:
            user = form.save(commit=False)
            user.username = form.cleaned_data.get('email')
            user.user_verified = False
            user.save()
            return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, 'Помилка реєстрації')
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_errors'] = context['form'].errors
        return context


class VerificationView(View):
    template_name = 'main/verification.html'
    model = get_user_model()

    def get(self, request, *args, **kwargs):
        # Send verification code
        user = request.user
        email = user.email
        user.verification_code = random.randint(100000, 999999)
        request.user.save()
        send_mail(
            'Код веритифікації',
            f'{user.verification_code} - Ваш верифікаційний код.',
            settings.DEFAULT_FROM_EMAIL,
            [email],
            fail_silently=False,
        )
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        code = request.POST.get('code')
        if request.user.is_authenticated:
            if request.user.verification_code == code:
                request.user.user_verified = True
                request.user.save()
                request.user.refresh_from_db()
                messages.success(request, 'Пора випити пива!!!!')
                return redirect('office')
            else:
                messages.error(request, 'Невірний код верифікації')
                return redirect('verification')
        else:
            messages.error(request, 'Користувач не авторизований')
            return redirect('verification')


@login_required()
def office(request):
    delete_old_data()
    user = request.user
    if user.is_authenticated:
        user.user_ip = request.META.get('HTTP_X_REAL_IP')
        if user.user_ip:
            # Якщо є HTTP_X_FORWARDED_FOR, використовуйте перший IP зі списку
            user.user_ip = user.user_ip.split(',')[0].strip()
        else:
            # Якщо немає HTTP_X_FORWARDED_FOR, використовуйте REMOTE_ADDR
            user.user_ip = request.META.get('REMOTE_ADDR')
        user.save()

        user_appointment = Appointments.objects.filter(user=user)
        user_order = Orders.objects.filter(user=user)
        user_review = UserReview.objects.filter(user=user)
        context = {'user': user, 'user_appointment': user_appointment, 'user_order': user_order}
        return render(request, 'main/office_patient.html', context=context)
    else:
        return redirect('login')


@login_required()
def review(request):
    user = f"{request.user.first_name} {request.user.last_name} {request.user.surname}"
    text = request.GET.get('text')
    rating = request.GET.get('rating')
    print('Запис у БД...')
    user_review = UserReview(
        user=user,
        text=text,
        rating=rating
    )
    user_review.save()
    request.user.user_send_review = True
    request.user.save()

    print('Дякуємо за відгук!')
    print(user, text, rating)
    return JsonResponse({'success': 'Дякуємо за відгук!'})


@login_required()
def LogoutView(reqest):
    logout(request)
    return redirect('login' + '?next=/')

