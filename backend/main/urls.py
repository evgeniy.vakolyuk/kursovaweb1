from django.urls import path
from django.contrib.auth import views as auth_views
from django.views.decorators.cache import cache_page

from . import views
from .views import *

urlpatterns = [
    path('admin/', views.admin, name='admin'),
    path('fillbd/', views.fillbd, name='fillbd'),

    path('', cache_page(60)(views.main), name='home'),
    path('about_us/', cache_page(60)(views.about_us), name='about_us'),
    path('news/', cache_page(20)(views.news), name='news'),
    path('news/<slug:news_slug>', cache_page(20)(views.show_news), name='show_news'),

    path('medicine/', cache_page(60)(views.medicine), name='medicine'),
    path('medicine/user_order/', views.user_order, name='user_order'),

    path('doctors/', cache_page(10)(views.doctors), name='doctors'),
    path('doctors/user_appointment/', views.user_appointment, name='user_appointment'),

    path('login/', auth_views.LoginView.as_view(template_name='main/login.html'), name='login'),
    path('signup/verification/', VerificationView.as_view(), name='verification'),
    path('office/', cache_page(5)(views.office), name='office'),
    path('signup/', SignupView.as_view(), name='signup'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('review/', views.review, name='review')
]
