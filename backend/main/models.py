from django.db import models
from django.urls import reverse
from django.contrib.auth.models import AbstractUser
from django.core.validators import EmailValidator
from phonenumber_field.modelfields import PhoneNumberField
from datetime import datetime, timedelta
from faker import Faker
from multiselectfield import MultiSelectField
from django.db.models.signals import post_save
from django.dispatch import receiver

from django.db import models
from django.core.files import File
from django.conf import settings
import os
import random


class News(models.Model):
    title = models.CharField(max_length=255, verbose_name='Заголовок')
    slug = models.SlugField(max_length=255, db_index=True, verbose_name='URL')
    photo = models.ImageField(max_length=255, verbose_name='Фотографія')
    content = models.TextField(verbose_name='Вміст')
    time_create = models.DateTimeField(auto_now_add=True, verbose_name='Дата публікації')
    is_published = models.BooleanField(default=True, verbose_name='Опубліковано')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('show_news', kwargs={'news_slug': self.slug})

    @staticmethod
    def generate_news_data():
        news = [
            {
                'title': 'В Україні за участі Світового банку проведуть модернізацію медзакладів у всіх областях',
                'photo': File(open(os.path.join(settings.MEDIA_ROOT, 'news/news_1.jpeg'), 'rb')),
                'content': '«Зміцнення системи охорони здоров’я та збереження життя» (HEAL Ukraine) – це потужний інфраструктурний проєкт, який Міністерство охорони здоров’я реалізує у співпраці зі Світовим банком», – наголосив заступник міністра, головний державний санітарний лікар Ігор Кузін. <br><br>Він повідомив, що на рівні МОЗ створено проєктний офіс з відновлення, який приймає пропозиції інфраструктурних проєктів для закладів охорони здоров’я, з-поміж яких будуть відібрані проєкти для реалізації. <br><br>Зазначається, що проєкт спрямований на посилене забезпечення медзакладів, зокрема відновлення, оновлення та модернізацію інфраструктури. Йдеться про відбудову пошкоджених обʼєктів медичної інфраструктури, оновлення приміщень і будівель та модернізацію медзакладів сучасним обладнанням для посилення енергонезалежності. <br><br>«Медзаклади усіх областей можуть подати заявки та отримати фінансування інфраструктурних проєктів та проєктів зі встановлення сонячних панелей», - наголошують у міністерстві. <br><br>Водночас зауважується, що інфраструктурні проєкти стосуються відновлення та модернізації пошкоджених внаслідок бойових дій державних або комунальних медзакладів, а також підвищення енергоефективності забезпечення доступу до альтернативних джерел енергії. <br><br>Окрім того, повідомляється, що окремо медзаклади первинної ланки медичної допомоги можуть подати заявку на встановлення сонячного енергетичного обладнання – сонячних панелей, акумулятора та інвертора, таким чином заклади зможуть використовувати сонячну енергію як автономне джерело живлення, накопичувати її та зменшити витрати на електроенергію.<br><br>«Тому кожен заклад, відповідно до критеріїв відбору, може оформити заявку на участь у проєкті та отримати можливість відновити або посилити інфраструктуру закладу охорони здоров’я», - резюмував Кузін. <br><br><em>Як повідомляв Укрінформ</em>, українська медична сфера матиме додаткову підтримку на суму 10 млн доларів як безповоротний грант, виділений у межах подальшого фінансування нового спільного зі Світовим банком проєкту МОЗ «Зміцнення системи охорони здоров’я та збереження життя» (HEAL Ukraine).',
                'is_published': True,
            },
            {
                'title': 'Наєв відвідав поранених військових',
                'photo': File(open(os.path.join(settings.MEDIA_ROOT, 'news/news_2.jpg'), 'rb')),
                'content': 'Командувач Об’єднаних Сил ЗСУ генерал-лейтенант Сергій Наєв відвідав один із лікувальних закладів Північної операційної зони, поспілкувався з медперсоналом та пораненими. Крім того, командувач нагородив військових відомчими відзнаками. <br><br><em>Як передає Укрінформ</em>, про це в Телеграмі повідомляє пресслужба Командування Об’єднаних Сил ЗСУ. <br><br>«Командувач Об’єднаних Сил ЗС України генерал-лейтенант Сергій Наєв відвідав один з лікувальних закладів Північної операційної зони, де проходять лікування поранені військовослужбовці. Боротьба з ворогом не припиняється по всій Україні. Вона триває і на лінії бойового зіткнення, і в тилу. Тут, у військових шпиталях на своєму фронті медики б’ються за життя і здоров’я кожного українського воїна. І роблять вони це успішно», - йдеться в повідомленні.',
                'is_published': True,
            },
            {
                'title': 'У Львові зафіксували два спалахи масових отруєнь',
                'photo': File(open(os.path.join(settings.MEDIA_ROOT, 'news/news_3.jpg'), 'rb')),
                'content': 'Львівська поліція розслідує два випадки харчового отруєння громадян з попередніми діагнозами «гостра кишкова інфекція» та «бактеріальна кишкова інфекція». <br><br><em>Про це повідомляється на сайті обласної поліції, передає Укрінформ.</em> <br><br>“З попередніми діагнозами «гостра кишкова інфекція» та «бактеріальна кишкова інфекція» у львівські лікарні госпіталізовано одинадцятеро осіб, з них – двоє дітей. Стан усіх пацієнтів стабільний”, - йдеться в повідомленні. <br><br>Як зазначається, 12 червня в поліцію надійшло повідомлення про те, що в одну з лікарень Львова з попереднім діагнозом «гостра кишкова інфекція» доставлено п’ятьох львів’ян - чотирьох дорослих та одну дитину. <br><br>За даними поліції, госпіталізовані громадяни в різний час харчувалися в одному з кафе, розташованому на проспекті Червоної Калини у Львові. <br><br>Того самого дня у поліцію надійшло ще одне повідомлення від медиків про госпіталізацію у профільну лікарню з попереднім діагнозом «бактеріальна кишкова інфекція» шістьох жителів міста – п’ятьох дорослих та одну дитину. <br><br>За даними правоохоронців, госпіталізовані громадяни вживали їжу під час банкету в одному з львівських ресторанів. <br><br>За цими фактами слідчі відкрили кримінальні провадження за ч.1 ст.325 (порушення санітарних правил і норм щодо запобігання інфекційним захворюванням та масовим отруєнням). <br><br>Санкція статті передбачає покарання у вигляді штрафу від тисячі до трьох тисяч неоподатковуваних мінімумів доходів громадян або арешт на строк до шести місяців, або обмеження волі на строк до трьох років, або позбавлення волі на той самий строк. <br><br>Поліцейські встановлюють обставини подій і проводять досудові розслідування. <br><br><em>Як повідомляв Укрінформ</em>, у травні на Вінниччині в Сугаківському ліцеї Вендичанської селищної ради Могилів-Подільського району було зафіксовано спалах гострої кишкової інфекції, в результаті чого постраждали 12 осіб.',
                'is_published': True,
            },
            {
                'title': 'У лікарнях Кривого Рогу залишаються 11 поранених',
                'photo': File(open(os.path.join(settings.MEDIA_ROOT, 'news/news_4.jpg'), 'rb')),
                'content': 'У лікарнях Кривого Рогу залишаються 11 людей, які отримали поранення внаслідок ворожих атак по місту.<br><br><em>Про це в Телеграмі повідомляє голова Дніпропетровської ОВА Сергій Лисак, передає Укрінформ.</em><br><br>"В лікарнях досі залишаються 11 поранених. 9 – важкі", - написав він.<br><br>Всього внаслідок атаки постраждали 38 людей, ще 12 - померли.<br><br>Раніше повідомлялось, що вночі у лікарні помер один з постраждалих від ракетної атаки на Кривий Ріг. 67-річний чоловік отримав опіки 80% тіла. Сьогодні у місті День жалоби.<br><br>Своєю чергою, міністр внутрішніх справ Ігор Клименко у Фейсбуці заявив, що рятувальники ДСНС працювали на зруйнованому ворожим ракетним ударом житловому будинку без перерв, доки не переконалися, що під завалами більше немає людей.<br><br>"Жорстокий, нелюдський і абсолютно безглуздий ракетний удар росіян по Кривому Рогу забрав життя ще однієї людини. Кількість загиблих зросла до 12. Окупанти 13 червня вдарили по житловому масиву, де лише багатоповерхівки, школи та дитячі майданчики. Лише в одному будинку знищено 80 квартир, в яких було зареєстровано 105 мирних громадян. Пошкодження отримали 68 будівель, серед них і навчальні заклади", - розповів він.<br><br>Ворог випустив шість ракет по Кривому Рогу, з яких п\'ять влучили по мирних об\'єктах, які не мають жодного відношення до військових. Ворог завдав удару по п\'ятиповерховому житловому будинку, також від ударів постраждали транспортне підприємство, складське приміщення, в якому зберігалася звичайна вода та різні напої, інші цивільні об\'єкти.',
                'is_published': True,
            },
        ]

        for data in news:
            news = News(**data)
            news.save()

    class Meta:
        verbose_name = 'Новина'
        verbose_name_plural = 'Новини'
        ordering = ['-time_create']


class CustomUser(AbstractUser):
    genders = {
        ('m', 'Чоловіча'),
        ('f', 'Жіноча'),
    }
    username = models.CharField(max_length=50, unique=True, null=True, verbose_name='Електронна адреса')
    surname = models.CharField(max_length=30, verbose_name='По батькові')
    gender = models.CharField(max_length=1, choices=genders, default='', verbose_name='Стать')
    birth_date = models.DateField(default='2023-01-01', verbose_name='Дата народження')
    phone_number = PhoneNumberField(region=None, unique=True, max_length=20, null=False, blank=False, verbose_name='Номер телефону')
    email = models.EmailField(unique=True, null=False, blank=False, verbose_name='Електронна адреса')
    verification_code = models.CharField(max_length=6, blank=True, null=True, verbose_name='Верифікаційний код')
    user_verified = models.BooleanField(max_length=1, blank=True, default=False, verbose_name='Користувач верифікований')
    user_send_review = models.BooleanField(max_length=1, blank=True, default=False, verbose_name='Користувач надсилав відгук')
    user_ip = models.GenericIPAddressField(unique=False, null=True, verbose_name='IP адреса')
    request_count = models.PositiveIntegerField(default=0, verbose_name='Кількість запитів на сервер')

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.surname}"


class Medicine(models.Model):
    locations = {
        ('1', 'Аптека "Подорожник", проспект Юності, 22, Вінниця, Вінницька область, 21000'),
        ('2', 'Аптека "Подорожник", вулиця Келецька, 106А, Вінниця, Вінницька область, 21000'),
        ('3', 'Аптека "Авіценна", проспект Юності, 21, Вінниця, Вінницька область, 21000'),
        ('4', 'Аптека "Авіценна", проспект Космонавтів, 42, Вінниця, Вінницька область, 21027'),
        ('5', 'Аптека "Конекс", вул, Хмельницьке шосе, 96-А, Вінниця, Вінницька область, 21000'),
        ('6', 'Аптека "Конекс", вулиця Келецька, 105, Вінниця, Вінницька область, 21000'),
    }
    medicine = models.CharField(max_length=100, verbose_name='Назва товару')
    photo = models.ImageField(max_length=255, verbose_name='Фотографія')
    description = models.TextField(verbose_name='Опис')
    volume = models.IntegerField(default=0, null=True, verbose_name='Об’єм')
    number_of_tablets = models.IntegerField(default=0, null=True, verbose_name='Кількість таблеток')
    weight = models.IntegerField(default=0, null=True, verbose_name='Вага')
    location = models.CharField(max_length=10, choices=locations, default='', verbose_name='Аптека та її адреса')
    location_url = models.URLField(null=False, verbose_name='Посилання на місцезнаходження Google Maps')
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Ціна')
    amount = models.IntegerField(verbose_name='Кількість')

    #def generate_fake_medicines(count):
    #    fake = Faker("uk_UA")
    #    default_photo_path = os.path.join(settings.MEDIA_ROOT, 'main/medicine.png')
    #    default_photo_file = File(open(default_photo_path, 'rb'))
    #    for _ in range(count):
    #        medicine = Medicine(
    #            medicine=fake.word(),
    #            photo=default_photo_file,
    #            description=fake.text(),
    #            location=fake.random_element(elements=['1', '2', '3']),
    #            price=fake.pydecimal(left_digits=3, right_digits=2, positive=True),
    #            amount=fake.random_int(min=1, max=100)
    #        )
    #        medicine.save()

    @staticmethod
    def generate_medicine_data():
        medicines = [
            {
                'medicine': 'Парацетамол',
                'photo': File(open(os.path.join(settings.MEDIA_ROOT, 'medicines/paracetamol.webp'), 'rb')),
                'description': 'Аналгетики та антипіретики. Парацетамол.',
                'volume': 0,
                'number_of_tablets': 10,
                'weight': 500,
                'location': '2',
                'location_url': 'https://goo.gl/maps/Eam1fS2fvsbwTJ339',
                'price': 19.52,
                'amount': 50
            },
            {
                'medicine': 'Активоване вугілля',
                'photo': File(open(os.path.join(settings.MEDIA_ROOT, 'medicines/aktivovane_vugillya.webp'), 'rb')),
                'description': 'Детоксикація, для органів ШКТ.',
                'volume': 0,
                'number_of_tablets': 5,
                'weight': 100,
                'location': '3',
                'location_url': 'https://goo.gl/maps/Fs7ifz8nhmqJJncc7',
                'price': 6,
                'amount': 132
            },
            {
                'medicine': 'Но-шпа таблетки',
                'photo': File(open(os.path.join(settings.MEDIA_ROOT, 'medicines/noshpa.webp'), 'rb')),
                'description': 'Засоби, які застосовуються при функціональних шлунково-кишкових розладах. ',
                'volume': 0,
                'number_of_tablets': 24,
                'weight': 400,
                'location': '6',
                'location_url': 'https://goo.gl/maps/oqREnbY7seF2V8iX7',
                'price': 72.15,
                'amount': 75
            },
            {
                'medicine': 'Валеріани настойка',
                'photo': File(open(os.path.join(settings.MEDIA_ROOT, 'medicines/valeriana.webp'), 'rb')),
                'description': 'Снодійні та седативні препарати.',
                'volume': 25,
                'number_of_tablets': 0,
                'weight': 0,
                'location': '4',
                'location_url': 'https://goo.gl/maps/XXPueZVeLEJHY1Ua8',
                'price': 30.42,
                'amount': 34
            },
            {
                'medicine': 'Магнікор',
                'photo': File(open(os.path.join(settings.MEDIA_ROOT, 'medicines/magnikor.webp'), 'rb')),
                'description': 'Антитромботичні засоби. Інгібітори агрегації тромбоцитів, за винятком гепарину.',
                'volume': 0,
                'number_of_tablets': 30,
                'weight': 500,
                'location': '1',
                'location_url': 'https://goo.gl/maps/fHVCtnfRidsk8pVL7',
                'price': 36,
                'amount': 29
            },
            {
                'medicine': 'Ундевіт драже',
                'photo': File(open(os.path.join(settings.MEDIA_ROOT, 'medicines/undevit.jpg'), 'rb')),
                'description': 'Полівітамінні комплекси без добавок. ',
                'volume': 0,
                'number_of_tablets': 50,
                'weight': 750,
                'location': '5',
                'location_url': 'https://goo.gl/maps/oK1SjBTQg8U87aS98',
                'price': 24.39,
                'amount': 78
            },
            {
                'medicine': 'Смекта порошок',
                'photo': File(open(os.path.join(settings.MEDIA_ROOT, 'medicines/smekta.webp'), 'rb')),
                'description': 'Протидіарейні препарати, що застосовуються при інфекційно-запальних захворюваннях кишечнику. Ентеросорбенти.',
                'volume': 0,
                'number_of_tablets': 0,
                'weight': 3000,
                'location': '3',
                'location_url': 'https://goo.gl/maps/Fs7ifz8nhmqJJncc7',
                'price': 169.49,
                'amount': 42
            },
            {
                'medicine': 'Оптикс',
                'photo': File(open(os.path.join(settings.MEDIA_ROOT, 'medicines/optiks.webp'), 'rb')),
                'description': 'Полівітамінні препарати з іншими добавками.',
                'volume': 0,
                'number_of_tablets': 60,
                'weight': 500,
                'location': '4',
                'location_url': 'https://goo.gl/maps/XXPueZVeLEJHY1Ua8',
                'price': 312,
                'amount': 21
            },
            {
                'medicine': 'Евказолін аква спрей',
                'photo': File(open(os.path.join(settings.MEDIA_ROOT, 'medicines/evkazolin_aqua.webp'), 'rb')),
                'description': 'Засоби, що застосовуються при захворюваннях порожнини носа. Протинабрякові та інші препарати для місцевого застосування при захворювання порожнини носа. Симпатоміметики, прості препарати.',
                'volume': 0,
                'number_of_tablets': 0,
                'weight': 10000,
                'location': '6',
                'location_url': 'https://goo.gl/maps/oqREnbY7seF2V8iX7',
                'price': 85.58,
                'amount': 0
            },
            {
                'medicine': 'Кардонат',
                'photo': File(open(os.path.join(settings.MEDIA_ROOT, 'medicines/kardonat.webp'), 'rb')),
                'description': 'Вітаміни у комбінації з різними речовинами.',
                'volume': 0,
                'number_of_tablets': 20,
                'weight': 750,
                'location': '3',
                'location_url': 'https://goo.gl/maps/Fs7ifz8nhmqJJncc7',
                'price': 286.27,
                'amount': 0
            },
        ]

        for data in medicines:
            medicine = Medicine(**data)
            medicine.save()

    def __str__(self):
        return self.medicine

    class Meta:
        verbose_name = 'Ліки'
        verbose_name_plural = 'Ліки'
        ordering = ['-amount']


class Orders(models.Model):
    user = models.CharField(max_length=30, verbose_name='Користувач')
    user_email = models.CharField(max_length=50, verbose_name='Електронна адреса')
    user_phonenumber = models.CharField(max_length=20, verbose_name='Номер телефону')
    order_location = models.CharField(max_length=150, verbose_name='Аптека')
    order_medicine = models.CharField(max_length=100, verbose_name='Ліки')
    order_amount = models.CharField(max_length=2, verbose_name='Кількість')
    order_price =  models.CharField(max_length=10, verbose_name='Загальна вартість')
    order_code = models.CharField(max_length=6, verbose_name='Код замовлення')
    time_create = models.DateTimeField(verbose_name='Дата замовлення')

    def __str__(self):
        return f"{self.user} - {self.order_location} - {self.order_medicine} - {self.order_amount}"

    class Meta:
        verbose_name = 'Замовлення'
        verbose_name_plural = 'Замовлення'


class Doctors(models.Model):
    specializations = {
        ('1', 'Дерматолог'),
        ('2', 'Кардіолог'),
        ('3', 'Психіатр'),
        ('4', 'Хірург'),
        ('5', 'Терапевт'),
        ('6', 'Алерголог'),
    }
    first_name = models.CharField(max_length=50, verbose_name='Імя')
    last_name = models.CharField(max_length=50, verbose_name='Призвіще')
    surname = models.CharField(max_length=50, verbose_name='По батькові')
    photo = models.ImageField(max_length=255, verbose_name='Фотографія')
    specialization = models.CharField(max_length=1, choices=specializations, verbose_name='Спеціалізація')
    meet_url = models.URLField(verbose_name='Посилання на прийом')
    rating = models.IntegerField(verbose_name='Оцінка')
    expiriance = models.IntegerField(verbose_name='Стаж')
    price = models.CharField(max_length=10, verbose_name='Вартість прийому')

    def __str__(self):
        return f"{self.last_name} {self.first_name} {self.surname}"

    @staticmethod
    def generate_fake_doctors(count):
        fake = Faker("uk_UA")
        default_photo_path = os.path.join(settings.MEDIA_ROOT, 'doctors/anonim.png')
        default_photo_file = File(open(default_photo_path, 'rb'))
        for i in range(count):
            doctor = Doctors(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                surname=fake.first_name_male() + 'ович',
                specialization=random.choice(['1', '2', '3', '4', '5', '6']),
                photo = default_photo_file,
                meet_url = 'https://meet.google.com',
                rating=random.randint(3, 5),
                expiriance = random.randint(1, 30),
                price = random.choice(['100', '200', '300', '400'])
            )

            doctor.save()

    class Meta:
        verbose_name = 'Лікар'
        verbose_name_plural = 'Лікарі'


class Schedule(models.Model):
    time_slots = {
        ('08', '08:00'),
        ('09', '09:00'),
        ('10', '10:00'),
        ('11', '11:00'),
        ('12', '12:00'),
        ('13', '13:00'),
        ('14', '14:00'),
        ('15', '15:00'),
        ('16', '16:00'),
        ('17', '17:00'),
        ('18', '18:00'),
        ('19', '19:00'),
        ('20', '20:00'),
    }
    all_time_slots = [choice[0] for choice in time_slots]

    doctor = models.ForeignKey(Doctors, on_delete=models.CASCADE, verbose_name='Лікар')
    monday = MultiSelectField(max_length=124, null=True, blank=True, choices=sorted(time_slots), default=all_time_slots, verbose_name='Понеділок')
    tuesday = MultiSelectField(max_length=124, null=True, blank=True, choices=sorted(time_slots), default=all_time_slots, verbose_name='Вівторок')
    wednesday = MultiSelectField(max_length=124, null=True, blank=True, choices=sorted(time_slots), default=all_time_slots, verbose_name='Середа')
    thursday = MultiSelectField(max_length=124, null=True, blank=True, choices=sorted(time_slots), default=all_time_slots, verbose_name='Четвер')
    friday = MultiSelectField(max_length=124, null=True, blank=True, choices=sorted(time_slots), default=all_time_slots, verbose_name='П`ятниця')
    saturday = MultiSelectField(max_length=124, null=True, blank=True, choices=sorted(time_slots), default=all_time_slots, verbose_name='Субота')
    sunday = MultiSelectField(max_length=124, null=True, blank=True, choices=sorted(time_slots), default=all_time_slots, verbose_name='Неділя')

    def __str__(self):
        return f'{self.doctor}'

    def generate_fake_schedule(count):
        doctors = Doctors.objects.all()  # Отримати всіх лікарів

        monday_values = ['08', '09', '10', '11', '13', '14', '15']
        tuesday_values = ['08', '09', '10', '11', '13', '14', '15']
        wednesday_values = ['09', '10', '11', '12', '14', '15', '16']
        thursday_values = ['09', '10', '11', '12', '14', '15', '16']
        friday_values = ['10', '11', '12', '13', '15', '16', '17']
        saturday_values = ['12', '13', '15', '16']
        sunday_values = ['12', '13', '14', '16']

        last_doctor_index = 0  # Зберігаємо індекс останнього вибраного доктора

        for _ in range(count):
            doctor = doctors[last_doctor_index]  # Вибираємо доктора за збереженим індексом
            last_doctor_index = (last_doctor_index + 1) % len(doctors)  # Оновлюємо індекс доктора

            schedule = Schedule(doctor=doctor)

            # Генерація випадкових дат для кожного дня тижня
            schedule.monday = sorted(random.sample(monday_values, random.randint(1, len(monday_values))))
            schedule.tuesday = sorted(random.sample(tuesday_values, random.randint(1, len(tuesday_values))))
            schedule.wednesday = sorted(random.sample(wednesday_values, random.randint(1, len(wednesday_values))))
            schedule.thursday = sorted(random.sample(thursday_values, random.randint(1, len(thursday_values))))
            schedule.friday = sorted(random.sample(friday_values, random.randint(1, len(friday_values))))
            schedule.saturday = sorted(random.sample(saturday_values, random.randint(1, len(saturday_values))))
            schedule.sunday = sorted(random.sample(sunday_values, random.randint(1, len(sunday_values))))

            schedule.save()

    class Meta:
        verbose_name = 'Розклад'
        verbose_name_plural = 'Розклади'


class Appointments(models.Model):
    user = models.CharField(max_length=30, verbose_name='Користувач')
    user_email = models.CharField(max_length=50, verbose_name='Електронна адреса')
    doctor = models.CharField(max_length=150, verbose_name='ПІБ лікаря')
    doctor_specialization = models.CharField(max_length=50, verbose_name='Спеціалізація')
    day = models.CharField(max_length=10, verbose_name='День')
    time = models.CharField(max_length=10, verbose_name='Час')
    meet_url = models.URLField(verbose_name='Посилання на зустріч')
    code = models.CharField(max_length=6, verbose_name='Код')
    time_create = models.DateTimeField(verbose_name='Дата запису')

    def __str__(self):
        return f"{self.user} - {self.doctor} - {self.day} - {self.time}"

    class Meta:
        verbose_name = 'Запис'
        verbose_name_plural = 'Записи'

class UserReview(models.Model):
    user = models.CharField(max_length=150, verbose_name='Користувач')
    text = models.TextField(max_length=1000, verbose_name='Текст')
    rating = models.IntegerField(verbose_name='Оцінка')

    def __str__(self):
        return self.user

    @staticmethod
    def generate_fake_reviews(count):
        fake = Faker("uk_UA")
        for i in range(count):
            first_name = fake.first_name()
            last_name = fake.last_name()
            surname = fake.first_name_male() + 'ович'
            user_review = UserReview(
                user=f"{first_name} {last_name} {surname}",
                text=fake.text(),
                rating=fake.random_int(min=3, max=5)
            )
            user_review.save()

    class Meta:
        verbose_name = 'Відгук'
        verbose_name_plural = 'Відгуки'
        ordering = ['-id']


class BlockedIP(models.Model):
    ip_address = models.GenericIPAddressField(unique=True)

    def __str__(self):
        return self.ip_address

    class Meta:
        verbose_name = 'Заблокований користувач'
        verbose_name_plural = 'Заблоковані користувачі'