$(document).ready(function () {
    $('.popup_link').click(function (e) {
        e.preventDefault();

        let action = $(this).data('action');
        let day = $(this).data('day');
        let doctor = $(this).data('doctor');
        let doctor_id = $(this).data('doctor_id');
        let doctor_specialization = $(this).data('doctor_specialization');
        let time = $(this).data('time');
        let meet_url = $(this).data('meet_url');

        // Відправити AJAX-запит до сервера
        $.ajax({
            type: 'GET',
            url: '/doctors/user_appointment',
            data: {
                action: action,
                day: day,
                doctor: doctor,
                doctor_id: doctor_id,
                doctor_specialization: doctor_specialization,
                time: time,
                meet_url: meet_url
            },
            success: function (response) {
                if (response.hasOwnProperty('error')) {
                    const errorContainer = document.getElementById('error-message-container');
                    errorContainer.innerText = response.error;
                    errorContainer.style.transform = 'translateY(150%)';
                    errorContainer.style.opacity = '1';
                    setTimeout(function () {
                        errorContainer.style.transform = 'translateY(-100%)';
                        errorContainer.style.opacity = '0';
                    }, 3000);
                } else if (response.hasOwnProperty('success')) {
                    const successContainer = document.getElementById('success-message-container');
                    successContainer.innerText = response.success;
                    successContainer.style.transform = 'translateY(150%)';
                    successContainer.style.opacity = '1';
                    setTimeout(function () {
                        successContainer.style.transform = 'translateY(-100%)';
                        successContainer.style.opacity = '0';
                    }, 3000);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log('Error: ' + textStatus + errorThrown);
            }
        });
    });
});