$(document).ready(function () {

    const errorContainer = document.getElementById('error-message-container');
    const popupLinks = $('.popup-link');
    const userId = popupLinks.data('user_id');
    const userVerified = popupLinks.data('user_verified');
    const body = $('body');
    const lockPadding = $('.lock-padding');
    const lockPaddingValue = window.innerWidth - $('main').outerWidth() + 'px';
    let unlock = true;

    const timeout = 100;

    if (popupLinks.length > 0) {
        popupLinks.each(function () {
            const popupLink = $(this);
            popupLink.on("click", function (e) {
                if (userId === 'None') {
                    errorContainer.innerText = 'Вам потрібно спочатку зареєструватись!';
                    showError();
                } else if (userVerified === 'True') {
                    const popupName = popupLink.attr('href').replace('#', '');
                    const currentPopup = $('#' + popupName);
                    popupOpen(currentPopup);
                } else {
                    errorContainer.innerText = 'Підтвердіть свою електронну адресу';
                    showError();
                }
                e.preventDefault();
            });
        });
    }

    const popupCloseIcon = $('.close-popup');
    if (popupCloseIcon.length > 0) {
        popupCloseIcon.on('click', function (e) {
            const el = $(this);
            popupClose(el.closest('.popup'));
            e.preventDefault();
        });
    }

    function popupOpen(currentPopup) {
        if (currentPopup && unlock) {
            const popupActive = $('.popup.open');
            if (popupActive.length > 0) {
                popupClose(popupActive, false);
            } else {
                bodyLock();
            }
        }
        currentPopup.addClass('open');
        currentPopup.on("click", function (e) {
            if (!$(e.target).closest('.popup_box').length) {
                popupClose($(e.target).closest('.popup'));
            }
        });
    }


    function popupClose(popupActive, doUnlock = true) {
        if (unlock) {
            popupActive.removeClass('open');
            if (doUnlock) {
                bodyUnLock();
            }
        }
    }

    function bodyLock() {
        console.log('Padding: ' + '+' + lockPaddingValue);
        if (lockPadding.length > 0) {
            lockPadding.each(function () {
                const el = $(this);
                el.css('padding-right', lockPaddingValue);
            });
        }
        body.css('padding-right', lockPaddingValue);
        body.addClass('lock');

        unlock = false;
        setTimeout(function () {
            unlock = true;
        }, timeout);
    }

    function bodyUnLock() {
        setTimeout(function () {
            if (lockPadding.length > 0) {
                lockPadding.each(function () {
                    const el = $(this);
                    el.css('padding-right', '0px');
                    console.log('Padding: ' + '-' + lockPaddingValue);
                });
            }
            body.css('padding-right', '0px');
            body.removeClass('lock');
        }, timeout);

        unlock = false;
        setTimeout(function () {
            unlock = true;
        }, timeout);
    }

    $(document).on('keydown', function (e) {
        if (e.which === 27) {
            const popupActive = $('.popup.open');
            popupClose(popupActive);
        }
    });

    (function () {
        if (!Element.prototype.closest) {
            Element.prototype.closest = function (css) {
                let node = this;
                while (node) {
                    if (node.matches(css)) return node;
                    else node = node.parentElement;
                }
                return null;
            };
        }
    })();

    (function () {
        if (!Element.prototype.matches) {
            Element.prototype.matches = Element.prototype.matchesSelector ||
                Element.prototype.webkitMatchesSelector ||
                Element.prototype.mozMatchesSelector ||
                Element.prototype.msMatchesSelector;
        }
    })();

    function showError() {
        errorContainer.style.transform = 'translateY(150%)';
        errorContainer.style.opacity = '1';
        setTimeout(function () {
            errorContainer.style.transform = 'translateY(-100%)';
            errorContainer.style.opacity = '0';
            }, 3000);
    }
});