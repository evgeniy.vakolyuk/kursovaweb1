const inputFields = document.querySelectorAll('.inputGroup input');

inputFields.forEach(input => {
  input.addEventListener('input', () => {
    if (input.value.trim() !== '') {
      input.classList.add('filled');
    } else {
      input.classList.remove('filled');
    }
  });
});

inputFields.forEach(input => {
  input.addEventListener('input', () => {
    if (input.checkValidity()) {
      input.classList.remove('invalid');
    } else {
      input.classList.add('invalid');
    }
  });
});