 $(window).on('load', function () {

     const mediaFiles = $('img');
     const preloader = document.getElementById('preloader');
     const main = $('main');
     let i = 0;

     mediaFiles.each(function (index, file) {
        if (file.complete) {
            // Зображення вже завантажене
            i++;
            if (i === mediaFiles.length) {
                preloader.classList.add('preloader--hide');
                main.css('visibility', 'visible');
            }
        } else {
            // Додати обробник події `load`
            file.onload = function () {
                i++;
                console.log(i);
                if (i === mediaFiles.length) {
                    preloader.classList.add('preloader--hide');
                    main.css('visibility', 'visible');
                }
            };
        }
    });
 });