$(document).ready(function () {

    // Введення кількості та обрахунок загальної вартості

    $('.product').each(function () {
        const product = $(this).closest('.product');
        const orderAmountInput = product.find('.order_amount');
        const orderPriceElement = product.find('.order_price');
        const priceString = orderPriceElement.attr('data-price');
        const pricePerUnit = parseFloat(priceString.replace(',', '.'));

        orderAmountInput.on('input', function () {
            const quantity = parseInt(orderAmountInput.val());
            const totalPrice = quantity * pricePerUnit;
            orderPriceElement.text(totalPrice.toFixed(2) + ' ₴');
        });

        product.find('.decrease').click(function () {
            const input = $(this).siblings('.order_amount');
            const value = parseInt(input.val());
            if (value > parseInt(input.attr('min'))) {
                input.val(value - 1).trigger('input');
            }
        });

        product.find('.increase').click(function () {
            const input = $(this).siblings('.order_amount');
            const value = parseInt(input.val());
            if (value < parseInt(input.attr('max'))) {
                input.val(value + 1).trigger('input');
            }
        });

        product.find('.popup_link').click(function (e) {
            e.preventDefault();

            let action = product.data('action');
            let order_location = $(this).data('order_location');
            let order_medicine = $(this).data('order_medicine');
            let order_amount = product.find('.order_amount').val();
            let order_price = product.find('.order_price').text();

            // Відправити AJAX-запит до сервера
            $.ajax({
                type: 'GET',
                url: '/medicine/user_order',
                data: {
                    action: action,
                    order_location: order_location,
                    order_medicine: order_medicine,
                    order_amount: order_amount,
                    order_price: order_price,
                },

                success: function (response) {
                    if (response.hasOwnProperty('error')) {
                        const errorContainer = document.getElementById('error-message-container');
                        errorContainer.innerText = response.error;
                        errorContainer.style.transform = 'translateY(150%)';
                        errorContainer.style.opacity = '1';
                        setTimeout(function() {
                            errorContainer.style.transform = 'translateY(-100%)';
                            errorContainer.style.opacity = '0';
                        }, 3000);
                    } else if (response.hasOwnProperty('success')) {
                        const successContainer = document.getElementById('success-message-container');
                        successContainer.innerText = response.success;
                        successContainer.style.transform = 'translateY(150%)';
                        successContainer.style.opacity = '1';
                        setTimeout(function() {
                            successContainer.style.transform = 'translateY(-100%)';
                            successContainer.style.opacity = '0';
                        }, 3000);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    console.log('Error: ' + textStatus + errorThrown);
                }
            });
        });
    });
});