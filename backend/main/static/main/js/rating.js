$(document).ready(function() {
  $('.review_rating').each(function() {
    var rating = $(this).data('rating');
    $(this)
      .find('.star:lt(' + rating + ')')
      .addClass('active');
  });

  $('.rating_stars_send .star').click(function() {
    const value = $(this).data('value');
    $('.review_rating_send').val(value);
    $(this).addClass('active');
    $(this).prevAll('.star').addClass('active');
    $(this).nextAll('.star').removeClass('active');
  });

  const initialValue = $('.review_rating_send').val();
  $('.rating_stars_send .star[data-value="' + initialValue + '"]').addClass('active');
});