$(document).ready(function () {
    $('#id_phone_number').inputmask({
        mask: '+380(99) 999-99-99',
        showMaskOnHover: false,
        showMaskOnFocus: true,
        placeholder: '_',
        // Видаляємо всі символи, крім цифр перед вставкою
        onBeforePaste: function (pastedValue, opts) {
            const processedValue = pastedValue.replace(/\D/g, '');
            return processedValue;
        }
    });
});