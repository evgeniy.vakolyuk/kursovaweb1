let selectedTabButton;
function assignSelectedTab() {
    if (window.matchMedia('(max-width: 425px)').matches) {
        selectedTabButton.querySelector('.tab_img').style.display = 'none';
        selectedTabButton.querySelector('.tab_img_selected').style.display = 'block';
        selectedTabButton.style.color = '#ff922d';
    }
};

window.addEventListener('DOMContentLoaded', function () {
    selectedTabButton = document.querySelectorAll('.tab_nav')[0];
    handleWindowSizeChange();
    window.addEventListener('resize', handleWindowSizeChange);
});

function handleWindowSizeChange() {
    const tabs = document.querySelectorAll('.tab_nav');
    const tabImg = document.querySelectorAll('.tab_img');
    const tabImgSelected = document.querySelectorAll('.tab_img_selected');

    if (window.matchMedia('(max-width: 425px)').matches) {
        for (let i = 0; i < tabs.length; i++) {
            tabs[i].querySelector('span').innerText = '';
        }
        for (let i = 0; i < tabImg.length; i++) {
            tabImg[i].style.display = 'block';
            tabImgSelected[i].style.display = 'none';
        }
    } else {
        tabs[0].querySelector('span').innerText = 'Мої замовлення';
        tabs[1].querySelector('span').innerText = 'Мої записи';
        tabs[2].querySelector('span').innerText = 'Особиста інформація';
        tabs[3].querySelector('span').innerText = 'Налаштування акаунту';
        for (let i = 0; i < tabImg.length; i++) {
            tabImg[i].style.display = 'none';
            tabImgSelected[i].style.display = 'none';
        }
    }
    assignSelectedTab();
}

function changeColor(event, target) {
    const tabs = document.querySelectorAll('.tab_nav');
    const tabImg = document.querySelectorAll('.tab_img');
    const tabImgSelected = document.querySelectorAll('.tab_img_selected');
    if (window.matchMedia('(max-width: 425px)').matches) {
        for (let i = 0; i < tabImg.length; i++) {
            tabs[i].style.color = '#111618';
            tabs[i].style.border = '2px solid #111618';
            tabImg[i].style.display = 'block';
            tabImgSelected[i].style.display = 'none';
        }
        const selectedIndex = Array.from(tabs).findIndex(tab => tab.dataset.target === target);
        if (selectedIndex !== -1) {
            tabs[selectedIndex].style.border = '2px solid #ff922d';
            tabImg[selectedIndex].style.display = 'none';
            tabImgSelected[selectedIndex].style.display = 'block';
            selectedTabButton = tabs[selectedIndex];
        }

    } else {
        for (let i = 0; i < tabs.length; i++) {
            tabs[i].style.color = '#111618';
            tabs[i].style.border = '2px solid #111618';
        }
        event.target.style.color = '#ff922d';
        event.target.style.border = '2px solid #ff922d';
        selectedTabButton = event.target;
    }
    assignSelectedTab();

    const tabBlocks = document.querySelectorAll('.tab_block');
    for (let i = 0; i < tabBlocks.length; i++) {
        tabBlocks[i].style.display = 'none';
    }
    const selectedTab = document.querySelector(target);
    selectedTab.style.display = 'block';
    event.preventDefault();
}

$(document).ready(function () {

// Надсилання AJAX відгуку

    $('.popup_link').click(function (e) {
        e.preventDefault();

        const text = $('.review_message_send').val();
        const rating = $('.review_rating_send').val();

        $.ajax({
            type: 'GET',
            url: '/review/',
            data: {
                text: text,
                rating: rating
            },
            success: function (response) {
                if (response.hasOwnProperty('error')) {
                    alert(response.error);
                    //$('#error-message').text(response.error).show();
                } else if (response.hasOwnProperty('success')) {
                    console.log(
                        'Success!' +
                        ' Text: ' + text +
                        ' Rating: ' + rating
                    );
                    const successContainer = document.getElementById('success-message-container');
                    successContainer.innerText = 'Дякуємо за відгук!';
                    successContainer.style.transform = 'translateY(150%)';
                    successContainer.style.opacity = '1';
                    setTimeout(function() {
                        successContainer.style.transform = 'translateY(-100%)';
                        successContainer.style.opacity = '0';
                        }, 3000);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log('Error: ' + textStatus + errorThrown);
            }
        });
    });
});