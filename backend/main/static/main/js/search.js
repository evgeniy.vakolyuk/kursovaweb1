$(document).ready(function () {
    const searchInputMedicine = $('#search-medicine');
    const medicineList = $('#medicine-list .medicine_card');
    const medicineSection = document.getElementById('medicine-list');

    const searchInputSpecialization = $('#search-specialization');
    const doctorList = $('#doctor-list .doctor_box');

    const searchLoader = $('#search-loader')
    const noResultsMessage = $('#no-results-message');
    console.log('Ready!');

    searchInputMedicine.on('input', function () {
        const searchTerm = searchInputMedicine.val().toLowerCase();

        searchLoader.show();
        console.log('Searching...');

        let foundResults = false;

        medicineList.each(function () {
            const medicine = $(this).attr('data-medicine');

            if (medicine.includes(searchTerm)) {
                $(this).show();
                medicineSection.style.display = 'grid';
                searchLoader.hide();
                foundResults = true;
            } else {
                $(this).hide();
            }
        });

        if (foundResults) {
            noResultsMessage.hide();
        } else {
            noResultsMessage.show().css({
                'display': 'flex',
                'justify-content': 'center',
                'align-items': 'center'
            });
            searchLoader.hide();
            medicineSection.style.display = 'flex';
        }
    });

    searchInputSpecialization.on('input', function () {
        const searchTerm = searchInputSpecialization.val().toLowerCase();

        searchLoader.show();
        console.log('Searching...');

        let foundResults = false;

        doctorList.each(function () {
            const specialization = $(this).attr('data-specialization');

            if (specialization.includes(searchTerm)) {
                $(this).show();
                searchLoader.hide();
                foundResults = true;
            } else {
                $(this).hide();
            }
        });

        searchLoader.hide();

        if (foundResults) {
            noResultsMessage.hide();
        } else {
            noResultsMessage.show().css({
                'display': 'flex',
                'justify-content': 'center',
                'align-items': 'center'
            });
            searchLoader.hide();
        }
    });

    searchInputMedicine.on('focus', function () {
        searchInputMedicine.val('');
        medicineList.show();
        noResultsMessage.hide();
    });

    searchInputSpecialization.on('focus', function () {
        searchInputSpecialization.val('');
        doctorList.show();
        noResultsMessage.hide();
    });

    searchLoader.hide();
});