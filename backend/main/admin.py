from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.safestring import mark_safe

from .models import *


# Register your models here.
@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'content', 'time_create', 'photo', 'is_published')
    list_display_links = ('id', 'title')
    search_fields = ('id', 'title')
    list_editable = ('is_published',)
    list_filter = ('is_published', 'time_create')
    prepopulated_fields = {'slug': ('title',)}


@admin.register(CustomUser)
class CustomUserAdmin(UserAdmin):
    model = CustomUser
    list_display = ('id', 'last_name', 'first_name', 'surname', 'email', 'user_ip', 'request_count')
    list_display_links = ('id', 'last_name')
    fieldsets = (
        ('Персональна інформація', {'fields': ('first_name', 'last_name', 'surname', 'gender', 'birth_date', 'email', 'phone_number')}),
        ('Дозволи', {'fields': ('is_active', 'is_staff', 'is_superuser')}),
        ('Додаткова інформація', {'fields': ('last_login', 'date_joined', 'user_verified', 'verification_code', 'user_send_review', 'user_ip', 'request_count')}),
    )
    readonly_fields = ('last_name', 'first_name', 'surname', 'gender', 'birth_date', 'email', 'phone_number', 'user_verified', 'verification_code', 'user_send_review', 'user_ip', 'request_count', 'last_login', 'date_joined')


@admin.register(UserReview)
class UserReviewsAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'rating', 'text')
    list_display_links = ('id', 'user')


@admin.register(Medicine)
class MedicineAdmin(admin.ModelAdmin):
    list_display = ('id', 'medicine', 'generate_object_photo', 'description', 'location', 'price', 'amount')
    list_display_links = ('id', 'medicine', 'generate_object_photo')
    search_fields = ('id', 'medicine', 'location', 'amount')
    list_editable = ('amount',)
    list_filter = ('location', 'price', 'amount')

    def generate_object_photo(self, object):
        if object.photo:
            return mark_safe(f"<img src='{object.photo.url}' width=50>")

    generate_object_photo.short_description = 'Фото ліків'


@admin.register(Orders)
class OrderAdmin(admin.ModelAdmin):
    list_display = (
    'id', 'user', 'user_email', 'user_phonenumber', 'order_location', 'order_medicine', 'order_amount', 'order_price', 'order_code', 'time_create')
    list_display_links = ('id', 'user')


@admin.register(Doctors)
class DoctorsAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'specialization', 'generate_object_photo')
    list_display_links = ('id', 'first_name')

    def generate_object_photo(self, object):
        if object.photo:
            return mark_safe(f"<img src='{object.photo.url}' width=50>")

    generate_object_photo.short_description = 'Фото лікаря'


@admin.register(Schedule)
class ScheduleAdmin(admin.ModelAdmin):
    list_display = ('id', 'doctor', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday')
    list_display_links = ('id', 'doctor')


@admin.register(Appointments)
class AppointmentsAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'user_email', 'doctor', 'doctor_specialization', 'day', 'time', 'meet_url', 'code', 'time_create')
    list_display_links = ('id', 'user', 'user_email')


@admin.register(BlockedIP)
class BlockedIPAdmin(admin.ModelAdmin):
    list_display = ('id', 'ip_address')
    list_display_links = ('id', 'ip_address')


admin.site.site_title = 'Autohosp'
admin.site.site_header = 'Адміністрування Autohosp'
