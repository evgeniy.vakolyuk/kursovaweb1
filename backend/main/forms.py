from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django import forms
from datetime import date

from .models import *


class CustomUserCreationForm(UserCreationForm):
    current_year = date.today().year
    birth_date = forms.DateField(
        widget=forms.SelectDateWidget(
            attrs={'class': 'input_date'},
            years=range(current_year - 100, current_year + 1)
        ),
        label='Дата народження',
        initial = date(2000, 1, 1)
    )
    class Meta:
        model = CustomUser
        fields = ('last_name', 'first_name', 'surname', 'email', 'phone_number', 'gender', 'birth_date')
        widgets = {
            'last_name': forms.TextInput(attrs={'autofocus': True,
                                       'class': 'input',
                                       'required': '',
                                       'autocomplete': 'off'
                                       }),
            'first_name': forms.TextInput(attrs={'autofocus': True,
                                       'class': 'input',
                                       'required': '',
                                       'autocomplete': 'off'
                                       }),
            'surname': forms.TextInput(attrs={'autofocus': True,
                                       'class': 'input',
                                       'required': '',
                                       'autocomplete': 'off'
                                       }),
            'email': forms.EmailInput(attrs={'autofocus': True,
                                       'class': 'input',
                                       'required': '',
                                       'autocomplete': 'off'
                                       }),
            'phone_number': forms.NumberInput(attrs={'autofocus': True,
                                        'class': 'input',
                                        'required': '',
                                        'autocomplete': 'off',
                                        'minlength': '13',
                                       }),
            'gender': forms.RadioSelect(attrs={'autofocus': True,
                                       'class': 'input',
                                       'required': '',
                                       'autocomplete': 'off'
                                       }),
            'birth_date': forms.DateInput(attrs={'autofocus': True,
                                       'class': 'input',
                                       'required': '',
                                       'autocomplete': 'off'
                                       }),
        }


class CustomAuthenticationForm(AuthenticationForm):
    username = forms.EmailField(
        label='Введіть електронну адресу',
        widget=forms.EmailInput(attrs={'autofocus': True,
                                       'class': 'input',
                                       'required': '',
                                       'autocomplete': 'off'
                                       })
    )
    password = forms.CharField(
        label="Введіть пароль",
        strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'current-password', 'class': 'input'}),
    )
